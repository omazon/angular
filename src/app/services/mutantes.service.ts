import { Injectable } from '@angular/core';
import {Http} from '@angular/http';

@Injectable()
export class MutantesService {
    mutantes= [];
  constructor(private http: Http) {
    console.log('servicios de mutantes listos');
    this.cargar_mutantes();
  }
  cargar_mutantes() {
    this.http.get('assets/data/mutantes.json').subscribe(respuesta => {
            const data = respuesta.json();
            this.mutantes = data.mutantes;
        });
    }
}
